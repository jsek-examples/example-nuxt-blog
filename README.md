# nxt - example Nuxt.js 2.x Blog (Dev.to API)

Demo on [devto-nxt.netlify.app](https://devto-nxt.netlify.app/)

## Run Locally

```bash
$ npm install
$ npm run dev
```

## About

### Special ingredients

- Google fonts loader
- Pug, TypeScript, Vuetify
- Lazy loading more content
- Random gradients based on tags

### Known issues

- Article content markdown sometimes contains macros

### Considered follow-up

- switch to GraphCMS
- search, comments, PT Mono
- recently opened (sidebar)
