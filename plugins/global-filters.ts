import Vue from 'vue'

function cutAfter(text: string, limit: number) {
  return text.includes(' ')
    ? text.substr(0, (text + ' ').lastIndexOf(' ', limit))
    : text
}

const filters = {
  //
  cutAfter,
}

Object.entries(filters) // register all
  .forEach(([key, filter]) => Vue.filter(key, filter))
