import colors from 'vuetify/es5/util/colors'

export default {
  ssr: false,
  target: 'static',

  head: {
    titleTemplate: '%s - NXT',
    title: '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  plugins: [
    // Plugins to load before mounting the App
    '@/assets/main.sass',
    '~/plugins/vue-observe-visibility.js',
    '~/plugins/vue-markdown.js',
    '~/plugins/random-gradient.js',
    '~/plugins/global-filters.ts',
  ],

  components: true,

  buildModules: ['@nuxt/typescript-build', '@nuxtjs/vuetify'],

  modules: [
    '@nuxtjs/axios',
    [
      '@rainmakerdigital/nuxt-google-fonts',
      {
        google: {
          families: ['Exo+2:200,400,700', 'Montserrat:200,400,700'],
          downloadFonts: true,
        },
      },
    ],
  ],

  vuetify: {
    treeShake: true,
    customVariables: ['~/assets/variables.sass'],
    theme: {
      dark: true,
      options: {
        customProperties: true,
      },
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  build: {},
}
